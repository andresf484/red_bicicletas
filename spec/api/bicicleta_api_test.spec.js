var Bicicleta = require("../../models/bicicleta.model");
var request = require("request");
var server = require("../../bin/www");

var base_url = 'http://localhost:3000/api/bicicletas'

describe("Testing Bicicletas", function () {
    beforeEach(function (done) {
        var mongoDB = "mongodb://localhost/testAPI";
        mongoose.connect(mongoDB, {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true,
        });

        const db = mongoose.connection;
        db.on("error", console.error.bind(console, "connection error: "));
        db.once("open", function () {
            console.log("We are connected to test database!");
            done();
        });
    });

    afterEach(function (done) {
        bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe("GET BICICLETAS /", () => {
        it("Status 200 \n", (done) => {
            request.get(base_url, function (error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(bicicleta.length).toBe(3);
                done();
            });
        });
    });

    describe("POST BICICLETAS /create", () => {
        it("Status 200", (done) => {
            var headers = { "Content-Type": "application/json" };
            var aBici = '{"code": 1, "color": "verde", "modelo": "urbano", "lat": -34.5, "lng": -54.2}';
            request.post(
                {
                    headers: headers,
                    url: base_url + "/create",
                    body: aBici
                },
                function (error, response, body) {
                    expect(response.statusCode).toBe(200);

                    var bici = JSON.parse(aBici);
                    console.log(bici);
                    expect(bici.color).toBe("verde");
                    expect(bici.modelo).toBe("urbano");
                    expect(bici.lat).toBe(-34.5);
                    expect(bici.code).toBe(1);
                    done();
                }
            );
        });
    });
});

/* beforeEach(function(){console.log('testeando…'); });

describe("Bicicleta API", () => {
    describe("GET BICICLETAS /", () => {
        it("status 200", () => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1, "rojo", "urbana", [4.715126, -74.080835]);
            Bicicleta.add(a);
            request.get("http://localhost:3000/api/bicicletas", function (
                error,
                response,
                body
            ) {
                expect(response.statusCode).toBe(200);
            });
        });
    });
});

describe("POST BICICLETAS /create", () => {
    it("STATUS 200", (done) => {
        var headers = { "Content-Type": "application/json" };
        var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54 }';
        request.post(
            {
                headers: headers,
                url: "http://localhost:3000/api/bicicletas/create",
                body: aBici,
            },
            function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
            }
        );
    });
}); */

/* describe("POST BICICLETAS /create", () => {
    it("STATUS 200", (done) => {
            var headers = { "Content-Type": "application/json" };
            var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54 }';
            request.post(
                {
                    headers: headers,
                    url: "http://localhost:3000/api/bicicletas/create",
                    body: aBici
                }, 
                function (error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(aBici);
                    console.log(bici);
                    expect(bici.color).toBe('rojo');
                    expect(bici.modelo).toBe('urbana');
                    expect(bici.lat).toBe(-34);
                    expect(bici.lng).toBe(-54);
                    expect(bici.id).toBe(10);
                    done();
                }
            );
        });
}); */
