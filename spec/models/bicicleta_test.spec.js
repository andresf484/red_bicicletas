var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta.model");

describe('Testing Bicicletas \n', function() {
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error: '));
        db.once('open', function(){
            console.log('\n We are connected to test database! \n');
            done();
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe("Bicicletas.createInstance", () => {
        it("Crea una instancia de bicicleta", () => {
            var bici = Bicicleta.createInstance(
            1, 
            "verde", 
            "Urbana", 
            [-34.5, -54.1]
            );
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("Urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });

describe("Bicicletas.allBicis", () => {
    it("comienza vacia", (done) => {
        Bicicleta.allBicis(function (err, bicis) {
            expect(bicis.length).toBe(0);
            done();
        });
    });
});

    describe("Bicicletas.add", () => {
        it("agrega solo una bici", (done) => {
            var aBici = new Bicicleta({
                code: 1,
                color: "verde",
                modelo: "Urbana"
            });
            Bicicleta.add(aBici, function (err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });
        });
    });

    describe("Bicicletas.findByCode", () => {
        it("debe devolver la bicicleta con code 1", (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({
                    code: 1,
                    color: "verde",
                    modelo: "urbana"
                });
                Bicicleta.add(aBici, function (err, newBici) {
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({
                        code: 2,
                        color: "roja",
                        modelo: "urbana"
                    });
                    Bicicleta.add(aBici2, function (err, newBici2) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (error) {
                            expect(newBici.code).toBe(aBici.code);
                            console.log(newBici.code);
                            expect(newBici.color).toBe(aBici.color);
                            console.log(newBici.color);
                            expect(newBici.modelo).toBe(aBici.modelo);
                            console.log(newBici.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });

});    

/* beforeEach(() => {Bicicleta.allBicis=[];});

describe("Bicicleta.allBicis", () => {
    it("comienza vacio", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe("Bicicleta.add", () => {
    it("agregamos una", () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, "rojo", "urbana", [4.715126, -74.080835]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe("Bicicleta.findById", () => {
    it("debe devolver la bici con id 1", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBicis = new Bicicleta(1, "verde", "urbana");
        var aBicis2 = new Bicicleta(2, "rojo", "montaña");
        Bicicleta.add(aBicis);
        Bicicleta.add(aBicis2);
        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBicis.color);
        expect(targetBici.modelo).toBe(aBicis.modelo);
    });
});

describe("Bicicleta.removeById", () => {
    it("debe devolver la bici con id 1", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBicis = new Bicicleta(1, "verde", "urbana");
        var aBicis2 = new Bicicleta(2, "rojo", "montaña");
        Bicicleta.add(aBicis);
        Bicicleta.add(aBicis2);
        var targetBici = Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(1);
    });
}); */