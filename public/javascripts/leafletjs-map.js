var mymap = L.map('main_map').setView([4.711045, -74.079492], 15);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap);

/* L.marker([4.711776, -74.081952]).addTo(map);
L.marker([4.711562, -74.078495]).addTo(map);
L.marker([4.705219, -74.081946]).addTo(map); */

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function (result) {
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            //L.marker([bici.ubicacion, {title: bici.id}]).addTo(map);
            L.marker(bici.ubicacion,{title: 'ID: ' + bici.id + '\nModelo: ' + bici.modelo + '\nColor: ' + bici.color}).addTo(mymap);
        });
    }
});